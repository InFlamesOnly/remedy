//
//  ViewWithShadow.swift
//  Remedy
//
//  Created by macOS on 25.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class ViewWithShadow: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)

    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        layoutView()
    }
    
    func layoutView() {
        let containerView = UIView()
        
        layer.backgroundColor = UIColor.clear.cgColor
        layer.shadowColor = SHADOW_COLOR.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 8.0)
        layer.shadowOpacity = 0.4
        layer.shadowRadius = 6
        
        containerView.layer.cornerRadius = 6
        containerView.layer.masksToBounds = true
        
        self.addSubview(containerView)
    }
}
