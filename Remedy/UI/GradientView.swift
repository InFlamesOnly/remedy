//
//  GradientView.swift
//  Remedy
//
//  Created by macOS on 23.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class GradientView: UIView {

    let gradientLayer = CAGradientLayer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        defaultInitializer()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        defaultInitializer()
    }
    
    override func layoutSubviews() {
        defaultInitializer()
    }
    
    private func defaultInitializer() {
        addGradient()
    }
    
    private func addGradient () {
        gradientLayer.frame = bounds
        gradientLayer.colors = [FIRST_GRADIENT_COLOR.cgColor, SECOND_GRADIENT_COLOR.cgColor]
        gradientLayer.startPoint = CGPoint.init(x: 0, y: 0.5)
        gradientLayer.endPoint = CGPoint.init(x: 1, y: 0.5)
        layer.insertSublayer(gradientLayer, at: 0)
        layer.layoutIfNeeded()
    }
}
