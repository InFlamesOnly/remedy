//
//  GradientButton.swift
//  Remedy
//
//  Created by macOS on 23.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class GradientButton: UIButton {

    let gradientLayer = CAGradientLayer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        defaultInitializer()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        defaultInitializer()
    }
    
    
    func defaultInitializer() {
        setTitleColor(UIColor.white, for: .normal)
        round()
        addGradient()
    }
    
    func addGradient () {
        gradientLayer.frame = bounds
        gradientLayer.colors = [FIRST_GRADIENT_COLOR.cgColor, SECOND_GRADIENT_COLOR.cgColor]
        gradientLayer.startPoint = CGPoint.init(x: 0, y: 0.5)
        gradientLayer.endPoint = CGPoint.init(x: 1, y: 0.5)
        layer.insertSublayer(gradientLayer, at: 0)
        layer.layoutIfNeeded()
    }
    
    func round () {
        layer.cornerRadius = frame.height / 2
        layer.masksToBounds = true
    }
    
}
