//
//  AvatarView.swift
//  Remedy
//
//  Created by macOS on 25.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class AvatarView: UIView {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var initials: UILabel!
    @IBOutlet weak var showCamera: UIButton!
    
    var isAvatar : Bool!
    var fullName : String?
    var avatarPath : String?
    
    func loadView (frame : CGRect) ->  AvatarView {
        let view = Bundle.main.loadNibNamed("AvatarView", owner: self, options: nil)?.first as! AvatarView
        view.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height)
        self.addSubview(view)
        return view
    }
    
    func avatarWithGradient (bounds : CGRect) -> AvatarView {
        let view = AvatarView().loadView(frame: CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height))
        view.isAvatar = false
        view.fullName = "\(User.currentUser().name)"
//        view.fullName = "Максим Бачевский"
        view.config()
        return view
    }
    
    func config () {
        if isAvatar {
            //todo avatar from server
        } else {
            self.roundView()
            self.addShadow()
            self.addInitialsLabel()
            self.addGradientView()
        }
    }
    
    private func addGradientView () {
        let view = UIView(frame: self.frame)
        
        view.roundView()
        view.center = self.center
        view.viewWithGradientColors(firstColor: FIRST_GRADIENT_COLOR, secondColor: SECOND_GRADIENT_COLOR)
        self.insertSubview(view, at: 0)

    }
    
    private func addInitialsLabel () {
        let label = UILabel()
        label.text = self.getInitials(fullName: self.fullName!)
        label.textAlignment = .center
        label.font = UIFont(name: FONT_BOLD, size: 26)
        label.frame = CGRect(x:0,y:0,width:label.intrinsicContentSize.width,height:label.intrinsicContentSize.width)
        label.center = self.center
        label.textColor = UIColor.white
        self.insertSubview(label, at: 0)
    }
    
    func getInitials (fullName : String) -> String {
        let words = fullName.wordList
        var str = ""
        
        for  word in words {
            str = str + "\(word.first!)"
        }
        return str.uppercased()
    }
}

extension UIView {
    func viewWithGradientColors (firstColor : UIColor, secondColor : UIColor) {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [firstColor.cgColor, secondColor.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 0, y: 1)
        gradientLayer.frame = self.frame
        
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
}

extension String {
    var wordList: [String] {
        return components(separatedBy: CharacterSet.alphanumerics.inverted).filter { !$0.isEmpty }
    }
}
