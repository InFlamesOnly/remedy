//
//  RegistrationViewController.swift
//  Remedy
//
//  Created by macOS on 11/14/20.
//  Copyright © 2020 macOS. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class RegistrationViewController: ActivityViewController {
    
    @IBOutlet weak var nameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var surnameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var specialityTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var cityTextField: SkyFloatingLabelTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        nameTextField.delegate = self
        surnameTextField.delegate = self
        phoneTextField.delegate = self
        specialityTextField.delegate = self
        cityTextField.delegate = self
    }
    
    
    @IBAction func back () {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func registration () {
        if nameTextField.text?.count == 0 ||
            surnameTextField.text?.count == 0 ||
            phoneTextField.text?.count == 0 ||
            specialityTextField.text?.count == 0 ||
            cityTextField.text?.count == 0 {
            self.presentAlert(withTitle: "Помилка", message: "Поля мають бути заповнені")
        } else {
            self.start()
            RequestManager.shared.registration(name: nameTextField.text!, surname: surnameTextField.text!, phone: phoneTextField.text!, speciality: specialityTextField.text!, city: cityTextField.text!) { HTTPURLResponse in
                
                let alertController = UIAlertController(title: "Успіх", message: "Регістрація успішна!\n Дякую! Найближчим часом ми зв'яжемося з Вами", preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "OK", style: .default) { action in
                    self.dismiss(animated: true, completion: nil)
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion: nil)
                
            } failure: { errorCode in
                self.dismiss(animated: true, completion: nil)
            }
        }
    }

}

extension RegistrationViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == phoneTextField {
            if textField.text?.count == 0 {
                textField.text = TELEPHONECODE
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == phoneTextField {
            if textField.text == TELEPHONECODE {
                textField.text = ""
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneTextField {
            if range.location < 19 {
                return TelephoneNumberValidator.formattedTextField(textField, shouldChangeCharactersIn: range, replacementString: string)
            } else {
                return false
            }
        }
        return true
    }
}
