//
//  BonuseCell.swift
//  Remedy
//
//  Created by macOS on 9/29/20.
//  Copyright © 2020 macOS. All rights reserved.
//

import UIKit

class BonuseCell: UITableViewCell {
    
    @IBOutlet weak var bonusDescription : UILabel!
    @IBOutlet weak var bonusActive : UILabel!
    @IBOutlet weak var contentTextLabel : UILabel!
    @IBOutlet weak var date : UILabel!
    @IBOutlet weak var activeView : UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
