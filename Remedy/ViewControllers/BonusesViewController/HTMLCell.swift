//
//  HistoryFormsCell.swift
//  Remedy
//
//  Created by macOS on 9/29/20.
//  Copyright © 2020 macOS. All rights reserved.
//

import UIKit

class HTMLCell: UITableViewCell {
    
    @IBOutlet weak var htmlText : UILabel!
    @IBOutlet weak var bckView : UIView!

    
    func configure () {
        self.bckView.roundCorners(value: 6)
        self.bckView.addShadow()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
