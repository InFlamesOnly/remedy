//
//  BonusesViewController.swift
//  Remedy
//
//  Created by macOS on 9/29/20.
//  Copyright © 2020 macOS. All rights reserved.
//

import UIKit

class BonusesViewController: ActivityViewController, UITableViewDelegate, UITableViewDataSource {
    
    var formsArray = [Worksheets]()
    
    @IBOutlet weak var titleText : UILabel!
    @IBOutlet weak var useBonusButton : GradientButton!
    
    @IBOutlet weak var bonusTableView : UITableView!
    
    var bonusesURL = ""
    var howUseBonusesURL = ""
    
    var historyListString = ""
    var historyBonusString = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.startWithoutBlur()
        useBonusButton.round()
        bonusTableView.isHidden = true
        RequestManager.shared.getBonuses { (response, text, companyId, bonusesURL, howUseBonues) in
            self.bonusesURL = bonusesURL;
            self.howUseBonusesURL = howUseBonues;
            self.titleText.text = text
            
            self.formsArray = Worksheets.mapResponseToArrayObject(response: response)
            self.getHistoryListText()
            
        } failure: { errorCode in
            
        }
    }
    
    func getHistoryListText () {
        RequestManager.shared.getHistoryList { text in
            self.historyListString = text
            self.getHistoryBonusText()
        } failure: { errorCode in
            
        }
    }
    
    func getHistoryBonusText () {
        RequestManager.shared.getHistoryBonus { text in
            self.historyBonusString = text
            self.bonusTableView.isHidden = false
            self.bonusTableView.reloadData()
            self.stopWithoutBlur()
//            self.useBonusButton.defaultInitializer()
        } failure: { errorCode in
            
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.formsArray.count + 2
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "useBonus" {
            let vc = segue.destination as! BonusWebViewController
            vc.url = bonusesURL
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HTMLCell2") as? HTMLCell
            let htmlString = historyBonusString
            var attrStr: NSAttributedString? = nil
            do {
                if let data = htmlString.data(using: .unicode) {
                    attrStr = try NSAttributedString(data: data, options: [
                        .documentType : NSAttributedString.DocumentType.html
                    ], documentAttributes: nil)
                }
            } catch {
                
            }
            cell?.configure()
            cell?.htmlText.attributedText = attrStr
            cell?.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell!
        }

        if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HTMLCell") as? HTMLCell
            let htmlString = historyListString
            var attrStr: NSAttributedString? = nil
            do {
                if let data = htmlString.data(using: .unicode) {
                    attrStr = try NSAttributedString(data: data, options: [
                        .documentType : NSAttributedString.DocumentType.html
                    ], documentAttributes: nil)
                }
            } catch {
            }
            cell?.htmlText.attributedText = attrStr
            cell?.configure()
            cell?.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell!
        }

        if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TiitleCell")
            cell?.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell!
        }

        let form = formsArray[indexPath.row - 3]
        let cell = tableView.dequeueReusableCell(withIdentifier: PROGRESS_INFO_CELL_ID) as? ProgressInfoCell
        cell?.configure(worksheet: form)

        cell?.selectionStyle = UITableViewCell.SelectionStyle.none

        return cell!
    }
}
