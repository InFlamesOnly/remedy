//
//  BonusWebViewController.swift
//  Remedy
//
//  Created by macOS on 9/29/20.
//  Copyright © 2020 macOS. All rights reserved.
//

import UIKit

class BonusWebViewController: ActivityViewController {
    
    @IBOutlet weak var webView : UIWebView!
    
    var url : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.webView.isHidden = true
        self.startWithoutBlur()
        if self.navigationController is CustomNavigationController {
            let nc = self.navigationController as! CustomNavigationController
            nc.hideBurger()
        }
        
        self.webView.loadRequest(URLRequest(url: URL(string: self.url!)!))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.navigationController is CustomNavigationController {
            let nc = self.navigationController as! CustomNavigationController
            nc.showBurger()
        }
    }
    
    func setNavigationTitle () {
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedString.Key.font:
            UIFont(name: FONT, size: 22)!, NSAttributedString.Key.foregroundColor:UIColor.white]
        self.navigationController?.title = "МІЙ РЕЙТИНГ".uppercased()
    }
}

extension BonusWebViewController : UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.webView.isHidden = false
        self.stopWithoutBlur()
    }
}
