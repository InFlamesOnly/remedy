//
//  ProgressViewController.swift
//  Remedy
//
//  Created by macOS on 25.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

let PROGRESS_INFO_CELL_ID = "ProgressInfoCell"
let PROGRESS_CELL_ID = "ProgressCell"
let PROGRESS_SEPARATOR_CELL_ID = "ProgressSeparatorCell"

class ProgressViewController: ActivityViewController {
    
    @IBOutlet weak var progressTableView : UITableView!
    var worksheets : Array<Worksheets> = Array()
    
    var bonusesText = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.progressTableView.isHidden = true
        self.getInfo()
    }
    
    func getInfo () {
        self.startWithoutBlur()
        RequestManager.shared.worksheetsInfo(success: { (responseObject) in
            self.worksheets = Worksheets.mapResponseToArrayObject(response: responseObject)
            self.stopWithoutBlur()
            self.progressTableView.reloadData()
            self.progressTableView.isHidden = false
        }, bonusesText: { (bonusessText) in
            self.bonusesText = bonusessText
            print("\(bonusessText)")
        }) { (errorCode) in
            self.stopWithoutBlur()
            self.progressTableView.isHidden = false

        }
//        RequestManager.shared.researchList(success: { (responseObject) in
//            self.researches = Research.mapResponseToArrayObject(response: responseObject)
//            self.stopWithoutBlur()
//            self.researchedTableView.reloadData()
//            self.researchedTableView.isHidden = false
//        }) { (errorCode) in
//            self.stopWithoutBlur()
//            self.researchedTableView.isHidden = false
//        }
    }
    
    
}

extension ProgressViewController : UITableViewDelegate {
    
}

extension ProgressViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.worksheets.count + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PROGRESS_INFO_CELL_ID, for: indexPath) as! ProgressInfoCell
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: PROGRESS_CELL_ID) as! ProgressCell
            cell.configure(text: bonusesText)
            return cell
        }
        if indexPath.row == 1 {
            return tableView.dequeueReusableCell(withIdentifier: PROGRESS_SEPARATOR_CELL_ID) as! ProgressSeparatorCell
        }
        
        let worksheet = self.worksheets[indexPath.row - 2]
        cell.configure(worksheet: worksheet)
        
        return cell
    }
}
