//
//  CustomNavigationController.swift
//  Remedy
//
//  Created by macOS on 22.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import KYDrawerController

class CustomNavigationController: UINavigationController {
    
    var titleText = ""
    private let burgerButton = UIButton()
    private var gradientIsNil : Bool?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.crateBurgerButton()
//        self.createNavigationItems()
        self.configureTittle()
//        self.navigationBar.barTintColor = .green
//        self.hidesBarsOnSwipe = false
//        navigationBar.barTintColor = .white
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//                self.setGradient()
//    }
    
    override func viewDidLayoutSubviews() {
        if self.gradientIsNil == nil {
            self.setGradient()
            self.gradientIsNil = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationBar.barTintColor = .white
    }
    
    func hideBurger () {
        self.burgerButton.isHidden = true
    }
    
    func showBurger () {
        self.burgerButton.isHidden = false
    }
        
    private func createNavigationItems () {
        let navigationItem = UINavigationItem()
//        let burgerIcon = self.createBurgerIcon()
        
//        navigationItem.leftBarButtonItem = burgerIcon
        
        self.navigationBar.items = [navigationItem]
        self.view.addSubview(navigationBar)
    }
    
    private func configureTittle () {
        self.navigationBar.tintColor = .white
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.font:
                                                    UIFont(name: FONT, size: titleText == "Задати питання експерту" ? 16 : 22)!,
                                                  NSAttributedString.Key.foregroundColor:UIColor.white]
        self.checkFirstCreateNavigation()
        self.navigationBar.topItem!.title = titleText.uppercased()
    }
    
    private func checkFirstCreateNavigation () {
        if titleText == "" {
            titleText = "ДОСЛІДЖЕННЯ"
//            titleText = "ЛЕНТА"
        }
    }
    
    private func setGradient () {
        var colors = [UIColor]()
        colors.append(FIRST_GRADIENT_COLOR)
        colors.append(SECOND_GRADIENT_COLOR)
        self.navigationBar.setGradientBackground(colors: colors)
    }
    
    func crateBurgerButton () {
        let icon = UIImage(named: "Burger")
        burgerButton.addTarget(self, action: #selector(showMenu), for: .touchUpInside)
        burgerButton.imageView?.contentMode = .scaleAspectFill
        burgerButton.setImage(icon, for: .normal)
        self.navigationBar.addSubview(burgerButton)
        burgerButton.clipsToBounds = true
        burgerButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            burgerButton.leftAnchor.constraint(equalTo: navigationBar.leftAnchor, constant: 0),
            burgerButton.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor, constant: 2)
            ,
            burgerButton.heightAnchor.constraint(equalToConstant: 52),
            burgerButton.widthAnchor.constraint(equalToConstant: 72),
        ])
        burgerButton.contentMode = .scaleAspectFit
    }
    
//    private func createBurgerIcon () -> UIBarButtonItem {
//        let icon = UIImage(named: "Burger")
//        let iconButton = UIButton(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 40, height: 40)))
//        iconButton.setImage(icon, for: .normal)
//        let barButton = UIBarButtonItem(customView: iconButton)
//        iconButton.addTarget(self, action: #selector(showMenu), for: .touchUpInside)
//        iconButton.imageView?.contentMode = .scaleAspectFill
//        return barButton
//    }
    
    @objc func showMenu() {
        print("show menu")
        let drawerController = self.parent as? KYDrawerController
        drawerController?.setDrawerState(KYDrawerController.DrawerState.opened, animated: true)
    }

}

extension CAGradientLayer {
    convenience init(frame: CGRect, colors: [UIColor]) {
        self.init()
        self.frame = frame
        self.colors = []
        for color in colors {
            self.colors?.append(color.cgColor)
        }
        startPoint = CGPoint(x: 0, y: 0)
        endPoint = CGPoint(x: 0, y: 1)
    }
    
    func createGradientImage() -> UIImage? {
        
        var image: UIImage? = nil
        UIGraphicsBeginImageContext(bounds.size)
        if let context = UIGraphicsGetCurrentContext() {
            render(in: context)
            image = UIGraphicsGetImageFromCurrentImageContext()
        }
        UIGraphicsEndImageContext()
        return image
    }
    
}

extension UINavigationBar {
    func setGradientBackground(colors: [UIColor]) {
        
        var updatedFrame = bounds
        updatedFrame.size.height += self.frame.origin.y
        let gradientLayer = CAGradientLayer(frame: updatedFrame, colors: colors)
        
        setBackgroundImage(gradientLayer.createGradientImage(), for: UIBarMetrics.default)
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.titleTextAttributes = [
                NSAttributedString.Key.foregroundColor: UIColor.white,
                NSAttributedString.Key.font: UIFont(name: FONT, size: 22)!]
            appearance.backgroundImage = gradientLayer.createGradientImage()
            self.standardAppearance = appearance
            self.scrollEdgeAppearance = self.standardAppearance
            self.tintColor = .white
        }
    }
}
