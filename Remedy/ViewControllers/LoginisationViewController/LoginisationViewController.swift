//
//  LoginisationViewController.swift
//  Remedy
//
//  Created by macOS on 23.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import FirebaseMessaging
import SpringIndicator

private let LOGIN_ID = "user_is_login"

class LoginisationViewController: ActivityViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView : UIWebView!
    
    @IBOutlet weak var choiseSegmentController : UISegmentedControl!
    
    @IBOutlet weak var viewPager: ViewPager!
    @IBOutlet weak var forgotPassword: UIButton!
    
    @IBOutlet weak var topLoginConstraint: NSLayoutConstraint!
    
    var loginSwipeView : LoginSwipeView?
    var rememberPasswordSwipeView : RememberPasswordView?
    var newPasswordSwipeView : NewPasswordView?
    
    var loadIndicator : SpringIndicator?
    
    var phone = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewPager.dataSource = self
        self.hideKeyboardWhenTappedAround()
        self.webView.loadRequest(URLRequest(url: URL(string:"https://remedy-ua.com/wp-login.php")!))
        self.addIndicator()
        self.webView.delegate = self
        if choiseSegmentController.selectedSegmentIndex == 0 {
            loadIndicator?.isHidden = true
            self.webView.isHidden = true
        }
        
        self.setSegmentStyle()
        
        if UIApplication.isDeviceWithSafeArea {
            topLoginConstraint.constant = 40
             //e.g. change the frame size height of your UITabBar
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.loadIndicator?.removeFromSuperview()
    }
    
    private func addIndicator () {
        self.loadIndicator = SpringIndicator()
        self.loadIndicator!.frame.size = CGSize(width: 100, height: 100)
        self.loadIndicator!.center = self.webView.center
        self.loadIndicator!.lineColor = FIRST_GRADIENT_COLOR
        self.view.addSubview(loadIndicator!)
        self.loadIndicator!.start()
    }
    
    func setSegmentStyle() {
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let titleTextAttributes2 = [NSAttributedString.Key.foregroundColor: FIRST_GRADIENT_COLOR]
        choiseSegmentController.setTitleTextAttributes(titleTextAttributes2, for: .normal)
        choiseSegmentController.setTitleTextAttributes(titleTextAttributes, for: .selected)
        
        choiseSegmentController.addTarget(self, action: #selector(self.onSegChange(_:)), for: .valueChanged)
    }
    
    @objc func onSegChange(_ sender: UISegmentedControl) {
        print ("index: ", sender.selectedSegmentIndex)

        if sender.selectedSegmentIndex == 0 {
            loadIndicator?.isHidden = true
            self.webView.isHidden = true
        }

        if sender.selectedSegmentIndex == 1 {
            loadIndicator?.isHidden = false
            self.webView.isHidden = false
        }
    }
    
    @IBAction func registration(_ sender: Any) {
        guard let url = URL(string: "http://remedy-ua.com/#contact") else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func rememberPassword(_ sender: Any) {
        self.rememberPassword()
        print("rememberPassword")
    }
    
    func rememberPassword () {
        self.forgotPassword.isHidden = true
        self.viewPager.scrollToPage(index: 2)
        self.loginSwipeView!.getPhone()
    }
}

extension LoginisationViewController : ViewPagerDataSource {
    func numberOfItems(viewPager: ViewPager) -> Int {
        return 3
    }
    
    func viewAtIndex(viewPager: ViewPager, index: Int, view: UIView?) -> UIView {
        switch index {
        case 0:
            guard (self.loginSwipeView != nil) else {
                self.loginSwipeView = LoginSwipeView().loadView(frame: self.viewPager.frame)
                self.loginSwipeView!.delegate = self
                return self.loginSwipeView!
            }
            return self.loginSwipeView!
        case 1:
            guard (self.rememberPasswordSwipeView != nil) else {
                self.rememberPasswordSwipeView = RememberPasswordView().loadView(frame: self.viewPager.frame)
                self.rememberPasswordSwipeView!.delegate = self
                return self.rememberPasswordSwipeView!
            }
            return self.rememberPasswordSwipeView!
        case 2:
            guard (self.newPasswordSwipeView != nil) else {
                self.newPasswordSwipeView = NewPasswordView().loadView(frame: self.viewPager.frame)
                self.newPasswordSwipeView!.delegate = self
                return self.newPasswordSwipeView!
            }
            return self.newPasswordSwipeView!
        default:
            return UIView()
        }
    }
}

extension LoginisationViewController : LoginSwipeViewDelegate {
    func login(phone : String, password : String) {
        self.auth(phone: phone, password: password)
    }
    
    func get (phone : String) {
        self.rememberPasswordSwipeView?.loginTextField.text = TelephoneNumberValidator.formattedNumber(number: phone)
        print("\(phone)")
    }
}

extension LoginisationViewController : RememberPasswordViewDelegate {
    func getPassword(phone: String) {
        self.getPasswordFromServer(phone: phone)
    }
    
    func back() {
        self.viewPager.scrollToPage(index: 1)
        self.forgotPassword.isHidden = false
    }
}

extension LoginisationViewController : NewPasswordViewDelegate {
    func save() {
        self.newPassword(phone: self.phone, code: self.newPasswordSwipeView!.codeTextField.text!, newPassword: self.newPasswordSwipeView!.newPasswordTextField.text!)
    }
}

extension LoginisationViewController {
    func auth (phone : String, password : String) {
        self.start()
        RequestManager.shared.auth(phone: phone.filterPhone(), password: password, success: { (responseObject) in
            User.remove()
            self.saveUser(responseObject: responseObject)
            self.performSegue(withIdentifier: LOGIN_ID, sender: self)
            self.stop()
            self.cleareDeviceId()
            self.setFCM()
        }) { (errorCode) in
            self.stop()
        }
    }
    
    private func cleareDeviceId () {
        if let deviceId = UserDefaults.standard.string(forKey: "deviceId") {
            RequestManager.shared.clearDeviceId(deviceId: deviceId, success: { (_) in
                
            }) { (_) in
                
            }
        }
    }
    
    private func setFCM () {
        if let token = Messaging.messaging().fcmToken {
            RequestManager.shared.setFCM(fcmToken: token, success: { (responseObject) in
                print("\(responseObject)")
            }) { (_) in
                
            }
        }
    }
    
    private func saveUser (responseObject: Dictionary<String, Any>) {
        let user = User()
        user.get(response: responseObject)
        user.save()
        user.isAuth(auth: true)
    }
    
    func getPasswordFromServer (phone : String) {
//        self.viewPager.scrollToPage(index: 3)
        self.start()
        self.phone = phone
        RequestManager.shared.forgotPassword(phone: phone.filterPhone(), success: { (responseObject) in
            self.viewPager.scrollToPage(index: 3)
            self.stop()
        }) { (errorCode) in
            self.stop()
        }
    }
    
    func newPassword (phone : String, code : String, newPassword : String) {
//        self.viewPager.scrollToPage(index: 1)
        self.start()
        RequestManager.shared.newPassword(phone: phone.filterPhone(), code: code, newPassword: newPassword, success: { (responseObject) in
            self.viewPager.scrollToPage(index: 1)
            self.forgotPassword.isHidden = false
            self.stop()
        }) { (errorCode) in
            self.stop()
        }
    }
}


extension UISegmentedControl {
    func imageWithColor(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width:  1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor);
        context!.fill(rect);
        let image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return image!
    }
}
