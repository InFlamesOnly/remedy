//
//  NewPasswordView.swift
//  Remedy
//
//  Created by macOS on 23.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

protocol NewPasswordViewDelegate : class {
    func save()
}

private enum Errors : String {
    case Code = "Введіть код активації!"
    case Password = "Введіть новий пароль!"
    case PasswordLow = "Пароль має бути не менше 6 символів!"
}

class NewPasswordView: UIView {
    
    weak var delegate : NewPasswordViewDelegate?

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var codeTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var newPasswordTextField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var newPasswordImageView: UIImageView!
    
    @IBOutlet weak var showPasswordButton: UIButton!
    
    func loadView (frame : CGRect) ->  NewPasswordView {
        let view = Bundle.main.loadNibNamed("NewPasswordView", owner: self, options: nil)?.first as! NewPasswordView
        view.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height)
        view.contentView.hideKeyboardWhenTappedAround()
        self.addSubview(view)
        return view
    }
    
    @IBAction func save(_ sender: Any) {
        if self.checkValidationFields() {
            self.delegate?.save()
        }
    }
    
    @IBAction func showPassword(_ sender: Any) {
        if self.showPasswordButton.isSelected {
            self.showPasswordButton.setImage(UIImage(named: "ShowPassword"), for: .normal)
            self.showPasswordButton.isSelected = false
            self.newPasswordTextField.isSecureTextEntry = true
        } else {
            self.newPasswordTextField.isSecureTextEntry = false
            self.showPasswordButton.setImage(UIImage(named: "ShowPasswordAcitve"), for: .normal)
            self.showPasswordButton.isSelected = true
        }
    }
    
    private func checkValidationFields () -> Bool {
        if self.codeTextField.text?.count == 0 {
            self.showError(message: Errors.Code.rawValue)
            return false
        }
        if self.newPasswordTextField.text?.count == 0 {
            self.showError(message: Errors.Password.rawValue)
            return false
        }
        if self.newPasswordTextField.text!.count < 6 {
            self.showError(message: Errors.PasswordLow.rawValue)
            return false
        }
        return true
    }
    
    private func showError (message : Errors.RawValue) {
        if let topController = UIApplication.topViewController() {
            topController.presentAlert(withTitle: "Помилка", message: message)
        }
    }
}

extension NewPasswordView : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == codeTextField {
            
        } else if textField == newPasswordTextField {
            self.newPasswordImageView.image = UIImage(named: "PasswordActive")
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == codeTextField {
            
        } else if textField == newPasswordTextField {
            self.newPasswordImageView.image = UIImage(named: "Password")
        }
    }
}
