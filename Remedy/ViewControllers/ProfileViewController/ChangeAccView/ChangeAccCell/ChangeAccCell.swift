//
//  ChangeAccCell.swift
//  Remedy
//
//  Created by macOS on 29.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class ChangeAccCell: UITableViewCell {

    @IBOutlet weak var name : UILabel!
    
    func configure (account : Account) {
        self.name.text = account.name
        self.selectionStyle = .none
    }

}
