//
//  ChangePasswordView.swift
//  Remedy
//
//  Created by macOS on 29.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

protocol ChangePasswordViewDelegate : class {
    func save(oldPassword : String, newPassword : String)
}

private enum Errors : String {
    //TODO неверный пароль
    case Password = "Введіть старий пароль!"
    case NewPassword = "Введіть новий пароль!"
    case NewPasswordLow = "Пароль має бути не менше 6 символів!"
}

class ChangePasswordView: UIView {
    
    weak var delegate : ChangePasswordViewDelegate?
    
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var showPasswordButton: UIButton!

    @IBOutlet weak var oldPasswordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var newPasswordTextField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var oldPasswordImageView: UIImageView!
    @IBOutlet weak var newPasswordImageView: UIImageView!
    
    func loadView (frame : CGRect) ->  ChangePasswordView {
        let view = Bundle.main.loadNibNamed("ChangePasswordView", owner: self, options: nil)?.first as! ChangePasswordView
        view.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height)
        view.contentView.hideKeyboardWhenTappedAround()
        view.contentView.roundCorners(value: 10)
        self.addSubview(view)
        return view
    }
    
    @IBAction func close(_ sender: Any) {
        self.removeFromSuperview()
        print("Close")
    }
    
    @IBAction func save(_ sender: Any) {
        if self.checkValidationFields() {
            self.delegate?.save(oldPassword: self.oldPasswordTextField.text!, newPassword: self.newPasswordTextField.text!)
        }
    }
    
    private func checkValidationFields () -> Bool {
        if self.oldPasswordTextField.text!.count == 0 {
            self.showError(message: Errors.Password.rawValue)
            return false
        }
        if self.newPasswordTextField.text?.count == 0 {
            self.showError(message: Errors.NewPassword.rawValue)
            return false
        }
        if self.newPasswordTextField.text!.count < 6 {
            self.showError(message: Errors.NewPasswordLow.rawValue)
            return false
        }
        return true
    }
    
    private func showError (message : Errors.RawValue) {
        if let topController = UIApplication.topViewController() {
            topController.presentAlert(withTitle: "Помилка", message: message)
        }
    }
    
    @IBAction func showPassword(_ sender: Any) {
        if self.showPasswordButton.isSelected {
            self.showPasswordButton.setImage(UIImage(named: "ShowPassword"), for: .normal)
            self.showPasswordButton.isSelected = false
            self.oldPasswordTextField.isSecureTextEntry = true
            self.newPasswordTextField.isSecureTextEntry = true
        } else {
            self.oldPasswordTextField.isSecureTextEntry = false
            self.newPasswordTextField.isSecureTextEntry = false
            self.showPasswordButton.setImage(UIImage(named: "ShowPasswordAcitve"), for: .normal)
            self.showPasswordButton.isSelected = true
        }
    }
}

extension ChangePasswordView : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == oldPasswordTextField {
            self.oldPasswordImageView.image = UIImage(named: "PasswordActive")
        } else if textField == newPasswordTextField {
            self.newPasswordImageView.image = UIImage(named: "PasswordActive")
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == oldPasswordTextField {
            self.oldPasswordImageView.image = UIImage(named: "Password")
        } else if textField == newPasswordTextField {
            self.newPasswordImageView.image = UIImage(named: "Password")
        }
    }
}
