//
//  ProfielCell.swift
//  Remedy
//
//  Created by macOS on 28.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class ProfielCell: UITableViewCell {
    
    @IBOutlet weak var avatarView : AvatarView?
    @IBOutlet weak var notificationSwitch : CustomSwitch?
    @IBOutlet weak var changeAcc : UIButton?
    
    @IBOutlet weak var name : UILabel?
    @IBOutlet weak var bonusess : UILabel?
    @IBOutlet weak var phone : UILabel?
    @IBOutlet weak var firstPlace : UILabel?
    @IBOutlet weak var secondPlace : UILabel?
    @IBOutlet weak var thirdPlace : UILabel?


    func configure () {
        self.name?.text = User.currentUser().name
        self.bonusess?.text = User.currentUser().bonuses
        self.phone?.text = User.currentUser().phone
        self.updateNotification()
    }
    
    func updateNotification () {
        if User.currentUser().notification == 1 {
            self.notificationSwitch?.setOn(on: true, animated: false)
//            self.notificationSwitch.
            self.notificationSwitch?.isOn = true
        } else {
            self.notificationSwitch?.setOn(on: false, animated: false)
            self.notificationSwitch?.isOn = false
        }
    }
    
    func configure (from places : Array <Place>) {
        switch places.count {
        case 1:
            self.firstPlace?.text = places[0].name
            self.firstPlace?.isHidden = false
            self.secondPlace?.isHidden = true
            self.thirdPlace?.isHidden = true
            break;
        case 2:
            self.firstPlace?.text = places[0].name
            self.secondPlace?.text = places[1].name
            self.firstPlace?.isHidden = false
            self.secondPlace?.isHidden = false
            self.thirdPlace?.isHidden = true
            break;
        case 3:
            self.firstPlace?.text = places[0].name
            self.secondPlace?.text = places[1].name
            self.thirdPlace?.text = places[2].name
            self.firstPlace?.isHidden = false
            self.secondPlace?.isHidden = false
            self.thirdPlace?.isHidden = false
            break;
        default:
            break;
        }
    }
}
