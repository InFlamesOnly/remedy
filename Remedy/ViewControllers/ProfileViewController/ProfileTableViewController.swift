//
//  ProfileTableViewController.swift
//  Remedy
//
//  Created by macOS on 28.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

private let CELL_ID = "ProfileCell"

class ProfileTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(ProfielCell.self, forCellReuseIdentifier: CELL_ID)
    }

//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 0
//    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID, for: indexPath) as! ProfielCell
//        if indexPath.row == 0 {
//            cell.avatarView!.addSubview(AvatarView().avatarWithGradient (bounds : cell.avatarView!.bounds))
//        }
        return cell
    }
}
