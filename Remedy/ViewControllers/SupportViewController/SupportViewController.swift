//
//  SupportViewController.swift
//  Remedy
//
//  Created by macOS on 28.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class SupportViewController: UIViewController {
    
    @IBOutlet weak var firstPhone : UIButton!
    @IBOutlet weak var secondPhone : UIButton!
    @IBOutlet weak var thirdPhone : UIButton!
    
    @IBOutlet weak var mail : UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func tapFirstPhone(_ sender: Any) {
        self.call(phone: (self.firstPhone.titleLabel!.text!.filterPhone()))
    }
    
    @IBAction func tapSecondPhone(_ sender: Any) {
        self.call(phone: (self.secondPhone.titleLabel!.text!.filterPhone()))
    }
    
    @IBAction func tapThirdPhone(_ sender: Any) {
        self.call(phone: (self.thirdPhone.titleLabel!.text!.filterPhone()))
    }
    
    @IBAction func tapToMail(_ sender: Any) {
        self.openMail(mail: (self.mail.titleLabel?.text)!)
    }
    
    func call (phone : String) {
        let url = URL(string: "tel://\(phone)")
        UIApplication.shared.open(url!, options: [:], completionHandler: nil)
    }
    
    func openMail (mail : String) {
        let url = URL(string: "mailto:\(mail)")
        UIApplication.shared.open(url!, options: [:], completionHandler: nil)
    }
}
