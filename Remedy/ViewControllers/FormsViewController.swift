//
//  FormsViewController.swift
//  Remedy
//
//  Created by macOS on 28.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

private let kPolicy = "getpolicyfordoctor?"
private let kResearch = "success_worksheet?"

class FormsViewController: ActivityViewController {
    
    @IBOutlet weak var webView : UIWebView!
    
    var url : String?
    var formurl : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.webView.isHidden = true
        self.startWithoutBlur()
        if self.navigationController is CustomNavigationController {
            let nc = self.navigationController as! CustomNavigationController
            nc.hideBurger()
        }
        
        self.webView.loadRequest(URLRequest(url: URL(string: self.url!)!))
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.navigationController is CustomNavigationController {
            let nc = self.navigationController as! CustomNavigationController
            nc.showBurger()
        }
    }
    
    func setNavigationTitle () {
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedString.Key.font:
            UIFont(name: FONT, size: 22)!, NSAttributedString.Key.foregroundColor:UIColor.white]
        self.navigationController?.title = "ДОСЛІДЖЕННЯ".uppercased()
    }
}

extension FormsViewController : UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.webView.isHidden = false
        self.stopWithoutBlur()
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        if request.url?.absoluteString == "\(kAPIServer)\(kPolicy)" ||
            request.url?.absoluteString == "\(kAPIServer)\(kResearch)"  {
            self.navigationController?.popViewController(animated: true)
        }
        return true
    }
}

