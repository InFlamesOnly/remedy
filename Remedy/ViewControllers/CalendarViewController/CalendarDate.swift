//
//  CalendarDate.swift
//  StarClub
//
//  Created by Hackintosh on 7/5/19.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import RealmSwift

class CalendarDate: Object {
    @objc dynamic var articleId = 0
    @objc dynamic var title = ""
    @objc dynamic var shortContent = ""
    @objc dynamic var fullContent = ""
    @objc dynamic var startDate = ""
    @objc dynamic var startTime = ""
    @objc dynamic var endDate = ""
    @objc dynamic var endTime = ""
    @objc dynamic var location = ""
    @objc dynamic var image = ""

    
    func getArticleFromServerResponse (response : Dictionary<String, Any>) {
        let result = (response as [String : AnyObject])["article"]
        
        self.articleId = result! ["article_id"] as? Int ?? 0
        self.title = result! ["title"] as? String ?? ""
        self.shortContent = result! ["short_content"] as? String ?? ""
        self.fullContent = result! ["full_content"] as? String ?? ""
        self.startDate = result! ["start_date"] as? String ?? ""
        self.startTime = result! ["start_time"] as? String ?? ""
        self.endDate = result! ["end_date"] as? String ?? ""
        self.endTime = result! ["end_time"] as? String ?? ""
        self.location = result! ["location"] as? String ?? ""
        self.image = result! ["img"] as? String ?? ""
    }
    
    class func mapResponseToArrayObject (response : Dictionary<String, Any>) -> Array<CalendarDate> {
        var array = Array<CalendarDate>()
        if let artArray = response["result"] as? [[String: Any]] {
            for articleDict in artArray {
                let article = CalendarDate()
                article.getArticleFromServerResponse(response: articleDict)
                array.append(article)
            }
        }
        return array
    }

}
