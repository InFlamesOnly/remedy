//
//  CalculatorVC.swift
//  StarClub
//
//  Created by macOS on 05.05.18.
//  Copyright © 2018 macOS. All rights reserved.
//

import UIKit

class CalculatorVC: UIViewController {
    
    @IBOutlet weak var rezultButton: GradientButton!
    
    @IBOutlet weak var weight: UITextField!
    @IBOutlet weak var hight: UITextField!
    
    @IBOutlet weak var vcTittle: UILabel!
    @IBOutlet weak var firstTextFieldWidthConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewDidLayoutSubviews() {
        self.rezultButton.defaultInitializer()
    }
    
    @IBAction func calculate(_ sender: Any) {
        if self.weight.text?.count == 0 && self.hight.text?.count == 0 {
            self.alert(message: "Заповніть всі поля")
            return
        }
        
        if self.weight.text!.contains(".") {
            self.weight.text! = self.weight.text!.replacingOccurrences(of: ".", with: ",", options: .literal, range: nil)
        }
        
        if self.hight.text!.contains(".") {
            self.hight.text! = self.hight.text!.replacingOccurrences(of: ".", with: ",", options: .literal, range: nil)
        }
        
        if self.weight.text?.count == 0 {
            self.alert(message: "Вкажіть масу тіла")
            return
        }
        if self.hight.text?.count == 0 {
            self.alert(message: "Вкажіть зріст")
            return
        }
        
        let numberFormatter = NumberFormatter()
        let weight = numberFormatter.number(from: self.weight.text!)
        let height = numberFormatter.number(from: self.hight.text!)
        
        if weight!.floatValue < 20 || weight!.floatValue > 300 {
            self.alert(message: "Вкажіть правильну масу тіла")
            return
        }
        
        if height!.floatValue < 50 || height!.floatValue > 300 {
            self.alert(message: "Вкажіть правильний зріст")
            return
        }
        
        self.alert(message: "\(String(Int(self.calculateRezult()))) \n \(self.textFromRezult(result: self.calculateRezult()))", title: "Результат")
        
        self.view.endEditing(true)
    }
    
    func calculateRezult ()  -> Float {
        view.endEditing(true)
        let numberFormatter = NumberFormatter()
        let weight = numberFormatter.number(from: self.weight.text!)
        let height = numberFormatter.number(from: self.hight.text!)
        
        let floatWeight = weight?.floatValue
        let floatHeight = height?.floatValue
        
        let a = floatHeight!/100 * floatHeight!/100
        let b = floatWeight!/a
    
        return b
    }
    
    func textFromRezult (result : Float)  -> String {
        var value = ""
        if result < 16 {
            value = "Виражений дефіцит маси тіла"
    
        }
        else if result > 16 && result < 18.5 {
            value = "Недостатня (дефіцит) маса тіла"
        }
        else if result > 18.5 && result < 24.99 {
            value = "Норма"
        }
        else if result > 25 && result < 30 {
            value = "Надмірна маса тіла (запобігання)"
        }
        else if result > 30 && result < 35 {
            value = "Ожиріння"
        }
        else if result > 35 && result < 40 {
            value = "Ожиріння різке"
        }
        else if result > 40 {
            value = "Дуже різке ожиріння"
        }
        return value
    }
}

extension Decimal {
    var floatValue:Float {
        return NSDecimalNumber(decimal:self).floatValue
    }
}

extension NSLayoutConstraint {
    
    func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint {
        
        NSLayoutConstraint.deactivate([self])
        
        let newConstraint = NSLayoutConstraint(
            item: firstItem!,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)
        
        newConstraint.priority = priority
        newConstraint.shouldBeArchived = shouldBeArchived
        newConstraint.identifier = identifier
        
        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
}
