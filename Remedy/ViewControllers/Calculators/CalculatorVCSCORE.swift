//
//  CalculatorVCSCORE.swift
//  StarClub
//
//  Created by macOS on 13.11.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

import UIKit

class CalculatorVCSCORE: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var rezultButton: GradientButton!
    
    @IBOutlet weak var genderDropDown : DropDown!
    @IBOutlet weak var ageTextField : UITextField!
    @IBOutlet weak var adLevelTextField : UITextField!
    @IBOutlet weak var smokeDropDown : DropDown!
    @IBOutlet weak var plazmTextField : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.genderDropDown.optionArray = ["Чоловічий", "Жіночий"]
        self.smokeDropDown.optionArray = ["Так", "Ні"]
        
        self.genderDropDown.didSelect{(selectedText , index ,id) in
            self.genderDropDown.placeholder = selectedText
            self.genderDropDown.textAlignment = .center
        }
        
        self.smokeDropDown.didSelect{(selectedText , index ,id) in
            self.smokeDropDown.placeholder = selectedText
            self.smokeDropDown.textAlignment = .center
        }
        self.viewDidLayoutSubviews()
    }
    
    override func viewDidLayoutSubviews() {
        self.rezultButton.defaultInitializer()
    }
    
    
    @IBAction func calculate(_ sender: Any) {
        var smoke = 0
        var gender = 0
        if  genderDropDown.placeholder == "Жіночий" || genderDropDown.text == "Жіночий" {
            gender = 1
        }
        
        if  smokeDropDown.placeholder == "Так" || smokeDropDown.text == "Так"{
            smoke = 1
        }
        
        
        if self.ageTextField.text?.count == 0 && self.adLevelTextField.text?.count == 0 && self.plazmTextField.text?.count == 0 {
            self.alert(message: "Заповніть всі поля")
            return
        }
        
        if self.ageTextField.text?.count == 0 {
            self.alert(message: "Вкажіть ваш вік")
            return
        }

        if self.adLevelTextField.text?.count == 0 {
            self.alert(message: "Вкажіть рівень систолічного АТ")
            return
        } else {
            if Double(self.adLevelTextField.text!)! < 100 || Double(self.adLevelTextField.text!)! > 180 {
                self.alert(message: "Рівень систолічного артеріального тиску повинен бути в межах від 100 до 180 мм рт.ст.!")
                return
            }
        }
        
        if self.plazmTextField.text?.count == 0 {
            self.alert(message: "Вкажіть холістерин плазми")
            return
        } else {
            if Double(self.plazmTextField.text!)! < 3 || Double(self.plazmTextField.text!)! > 8 {
                self.alert(message: "Рівень холестерину має бути в межах від 3 до 8 ммоль/л!")
                return
            }
        }
        
        let rezult = self.calculate(gender: gender, smoke: smoke, age: Int(self.ageTextField.text!)!, adLevel: Double(self.adLevelTextField.text!)!, plazma: Double(self.plazmTextField.text!)!)
        
        if rezult < 1 {
            self.alert(message: "\(Int(rezult)) % Низький ризик", title: "Результат")
        } else if rezult > 1 && rezult < 5 {
             self.alert(message: "\(Int(rezult)) % Середній ризик", title: "Результат")
        } else if rezult > 5 && rezult < 10 {
            self.alert(message: "\(Int(rezult)) % Високий ризик", title: "Результат")
        } else if rezult > 10 {
            self.alert(message: "\(Int(rezult)) % Дуже високий ризик", title: "Результат")
        }
        self.view.endEditing(true)
    }
    
//    Уровень суммарного СС риск по шкале SCORE:
//    менее 1% - низким.
//    от >1 до 5% - средний или умеренно повышенный.
//    от >5% до 10% - высокий.
//    >10% - очень высокий.
    
    func calculate (gender : Int, smoke : Int, age : Int, adLevel : Double, plazma : Double) -> Double {
        
        var alpha = 0.0
        var p = 0.0
        
        if gender == 1 {
            alpha = -28.7
            p = 6.23
        } else {
            alpha = -21.0
            p = 4.62
        }
        
        let cs0 = exp(-exp(alpha)*pow(Double(age) - 20.0, p))
        let cs10 = exp(-exp(alpha)*pow(Double(age) - 10.0, p))
        
        if gender == 1 {
            alpha = -30.0
            p = 6.42;
        } else {
            alpha = -25.7
            p = 5.47
        }
        
        let ncs0 = exp(-exp(alpha)*pow(Double(age) - 20.0, p))
        let ncs10 = exp(-exp(alpha)*pow(Double(age) - 10.0, p))
        
        
        var bchol = 0.24
        var bsbp = 0.018
        var bsm = Double(smoke) * 0.71
        let wc = bchol * (Double(plazma)-6.0) + bsbp * (Double(adLevel)-120.0) + bsm
        
        bchol = 0.02
        bsbp = 0.022
        bsm = Double(smoke)*0.63
        let wnc = bchol * (Double(plazma)-6.0) + bsbp * (Double(adLevel)-120.0) + bsm
        
        let cs = pow(cs0, exp(wc))
        var cs1 = pow(cs10, exp(wc))

        let ncs = pow(ncs0, exp(wnc))
        var ncs1 = pow(ncs10, exp(wnc))
        
        cs1 = cs1/cs
        ncs1 = ncs1/ncs
        
        let r = 1.0-cs1
        let r1 = 1.0-ncs1
        
        let result = (100.0*(r+r1))// ---- вот это результат

        return result
    }
}

extension Double {
    func roundTo(places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
