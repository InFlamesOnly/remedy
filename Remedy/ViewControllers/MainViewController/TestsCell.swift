//
//  TestsCell.swift
//  Remedy
//
//  Created by macOS on 24.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class TestsCell: UITableViewCell {
    
    @IBOutlet weak var testCollectionView : UICollectionView!
    
    func setupFlowLayaut () {
        let layout = UPCarouselFlowLayout()
        layout.itemSize = CGSize(width: UIScreen.main.bounds.width - 67, height: 220)
        layout.scrollDirection = .horizontal
        self.testCollectionView.collectionViewLayout = layout
    }

//    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//    }
//
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }

}
