//
//  DetailMainViewController.swift
//  Remedy
//
//  Created by Dima on 17.04.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
//import RichTextView

class DetailMainViewController: ActivityViewController, UIWebViewDelegate {
    
    @IBOutlet weak var mainTableView : UITableView!
    
    @IBOutlet weak var webView : UIWebView!
    
    var article : Article!
    var attrText = NSAttributedString()
    
    var enabledScrolling = false
    var requestIsSended = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationTitle()
        self.startWithoutBlur()
        self.webView.delegate = self
        self.webView.loadRequest(URLRequest(url: URL(string: self.article.link)!))
        self.requestIsSended = false
        self.webView.isHidden = true
        
        if self.navigationController is CustomNavigationController {
            let nc = self.navigationController as! CustomNavigationController
            nc.hideBurger()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.navigationController is CustomNavigationController {
            let nc = self.navigationController as! CustomNavigationController
            nc.showBurger()
        }
    }
    
    func setNavigationTitle () {
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedString.Key.font:
            UIFont(name: FONT, size: 22)!, NSAttributedString.Key.foregroundColor:UIColor.white]
        self.navigationController?.title = "СТРІЧКА".uppercased()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.enabledScrolling {
            let height = scrollView.frame.size.height
            let contentYoffset = scrollView.contentOffset.y
            let distanceFromBottom = scrollView.contentSize.height - contentYoffset
            if distanceFromBottom < height {
                if self.requestIsSended == false {
                    self.requestIsSended = true
                    RequestManager.shared.plusContent(article: self.article, success: { (response) in
                        
                    }) { (errorCode) in
                        
                    }
                    print(" you reached end of the table")
                }
            }
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.stopWithoutBlur()
        self.enabledScrolling = true
        self.requestIsSended = false
        self.webView.isHidden = false

    }
//    private func webViewDidFinishLoad(webView : UIWebView) {
//        self.stopWithoutBlur()
//        self.enabledScrolling = true
//        self.requestIsSended = false
//        self.webView.isHidden = false
//    }
    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 1
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        let cellIdentifier = "DetailMainCell"
//        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! DetailMainCell
//        cell.articleTittle.text = article.tittle
//        cell.htmlTextView = RichTextView(
//            input: article.content,
//            latexParser: LatexParser(),
//            font: UIFont(name: FONT, size: 18)!,
//            textColor: UIColor.black,
//            frame: CGRect.zero,
//            completion: nil
//        )
////        cell.articleHTMLText.attributedText = self.attrText
//        cell.selectionStyle = .none
//        return cell
//    }
}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

//extension String {
//    var html2AttributedString: NSAttributedString? {
//        return Data(utf8).html2AttributedString
//    }
//    var html2String: String {
//        return html2AttributedString?.string ?? ""
//    }
//}
