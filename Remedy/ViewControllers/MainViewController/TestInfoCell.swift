//
//  TestInfoCell.swift
//  Remedy
//
//  Created by macOS on 24.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class TestInfoCell: UICollectionViewCell {
    
    @IBOutlet weak var bckView : UIView!
    
    func configure () {
        self.bckView.roundCorners(value: 6)
        self.addShadow()
    }
}

