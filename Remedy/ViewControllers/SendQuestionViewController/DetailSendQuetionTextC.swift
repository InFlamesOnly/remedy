//
//  SendQuetionTextC.swift
//  StarClub
//
//  Created by macOS on 20.02.18.
//  Copyright © 2018 macOS. All rights reserved.
//

import UIKit

class DetailSendQuetionTextC: UITableViewCell {
    
    @IBOutlet weak var sendQuestionTittle: UILabel!
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var sendText: UITextView!
    @IBOutlet weak var sendButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        roundView.roundCorners(value: 6)
        self.customisationButton()
    }
    
    func customisationButton () {
        sendButton.layer.borderColor = FIRST_GRADIENT_COLOR.cgColor
        sendButton.layer.cornerRadius = 10
        sendButton.layer.borderWidth = 1
        sendButton.layer.cornerRadius = sendButton.frame.size.height/2
    }
}
