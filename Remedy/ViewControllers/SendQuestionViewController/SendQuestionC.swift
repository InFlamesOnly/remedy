//
//  LegalConsultationC.swift
//  StarClub
//
//  Created by macOS on 20.02.18.
//  Copyright © 2018 macOS. All rights reserved.
//

import UIKit

class SendQuestionC: UITableViewCell {
    
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var isPrivateImage: UIImageView!
    @IBOutlet weak var isPrivateHeight: NSLayoutConstraint!
    @IBOutlet weak var isPrivateWidth: NSLayoutConstraint!
    @IBOutlet weak var quetionTittle: UILabel!
    @IBOutlet weak var quetion: UILabel!
    @IBOutlet weak var answer: UILabel!
    @IBOutlet weak var answerTextView: UITextView!
    @IBOutlet weak var moreButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        roundView.roundCorners(value: 6)
    }
}
