//
//  SendQuetionVC.swift
//  StarClub
//
//  Created by macOS on 20.02.18.
//  Copyright © 2018 macOS. All rights reserved.
//

import UIKit

class DetailSendQuetionVC: ActivityViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {

    @IBOutlet weak var questionTableView: UITableView!
    var isNewQestion = false
    var isMyQuestion = false
    var selectedId = 0
    var textViewText = ""
    
    var legalConsultationArray : Array<LegalConsultation> = Array()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.questionTableView.isHidden = true
        
        questionTableView.rowHeight = UITableView.automaticDimension
        questionTableView.estimatedRowHeight = 300
        
        if isNewQestion {
            self.questionTableView.isHidden = false
            print("isNew")
        } else {
            self.getListFromServer()
            print("isNotNew")
        }
        
        if self.navigationController is CustomNavigationController {
            let nc = self.navigationController as! CustomNavigationController
            nc.hideBurger()
        }
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.navigationController is CustomNavigationController {
            let nc = self.navigationController as! CustomNavigationController
            nc.showBurger()
        }
    }
    
    func getListFromServer () {
        self.startWithoutBlur()
        RequestManager.shared.questionList(id: String(self.selectedId)) { (_ responseObject: Dictionary<String, Any>) in
            self.legalConsultationArray = LegalConsultation.mapResponseToArrayObject(response: responseObject)
            let lc = self.legalConsultationArray[0]
            self.isMyQuestion = lc.isMyQuestion
            self.questionTableView.isHidden = false
            self.questionTableView.reloadData()
            self.stopWithoutBlur()
        } failure: { (_ error: Int?) in
            self.stopWithoutBlur()
        }
    }
    
    @IBAction func send(_ sender: Any) {
        self.startWithoutBlur()
        var indexPath = IndexPath()
        if legalConsultationArray.count == 0 {
            indexPath = IndexPath(row: 0, section: 0)
        } else {
            let indexPathRow = self.questionTableView.numberOfRows(inSection: self.questionTableView.numberOfSections - 1)
            indexPath = IndexPath(row: indexPathRow - 1, section: 0)
        }
        
        let cell: DetailSendQuetionTextC = self.questionTableView.cellForRow(at: indexPath) as! DetailSendQuetionTextC
        cell.sendButton.isHidden = true
        
        RequestManager.shared.sendQestion(qestionId: self.selectedId, text: self.textViewText) { (_ responseObject: Dictionary<String, Any>) in
            cell.sendButton.isHidden = false
            self.alertWithAction(message: "Дякуємо за Ваше звернення. Термін відповіді – 3 робочі дні!", title: "")
            self.stopWithoutBlur()
        } failure: { (_ error: Int?) in
            cell.sendButton.isHidden = false
            self.stopWithoutBlur()
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (textView.text == "Я вводжу текст питання…") {
            textView.text = ""
            //optional
        }
        textView.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.textViewText = textView.text
        if (textView.text == "") {
            textView.text = "Я вводжу текст питання…"
        }
        textView.resignFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if legalConsultationArray.count == 0 {
            return 1
        } else if self.isMyQuestion {
            return legalConsultationArray.count  + 1
        } else if !self.isMyQuestion {
            return legalConsultationArray.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "DetailSendQuetionC"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! DetailSendQuetionC
        if indexPath.row == legalConsultationArray.count {
            let cellIdentifier = "DetailSendQuetionTextC"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! DetailSendQuetionTextC
//            cell.sendQuestionTittle.text = "Введите ваш вопрос"
            if self.isNewQestion {
                _ = Timer.scheduledTimer(withTimeInterval: 0.7, repeats: false) { (timer) in
                    cell.sendText.becomeFirstResponder()
                }
            }
            else {
//                if !self.isMyQuestion {
//                    cell.sendButton.isEnabled = false
//                } else {
//                    cell.sendButton.isEnabled = true
//                }
            }
            cell.selectionStyle = .none
            return cell
        }
        
//        if !isNewQestion {
//            cell.sendQuestionTittle.text = "Личный вопрос"
//        }
        
    
        let lc = self.legalConsultationArray[indexPath.row]
        
        
        if lc.isPrivate {
            cell.sendQuestionTittle.text = "Особисте питання (видне тільки вам)"
        } else {
            cell.sendQuestionTittle.text = "Загальне питання (бачене всім)"
        }
        
        cell.quetion.text = lc.text
        let attributedString = NSAttributedString(html: lc.answer)
        cell.answerTextView.attributedText = attributedString
        cell.answerTextView.font = UIFont(name: FONT_MEDIUM, size: 11.0)
        cell.answer.text = cell.answerTextView.text
        cell.answerTextView.isScrollEnabled = false
        
        if lc.isAnswered {
            cell.answer.text = lc.answer
        } else {
            cell.answer.text = "Експерт готує відповідь. Спасибі за очікування."
            cell.answerTextView.text = "Експерт готує відповідь. Спасибі за очікування."
        }
        
        cell.selectionStyle = .none
        return cell
    }
}
