//
//  DrawerViewController.swift
//  Remedy
//
//  Created by macOS on 22.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import KYDrawerController

class DrawerViewController: UIViewController {
    
    @IBAction func closeDrawer(_ sender: Any) {
        let drawerController = navigationController?.parent as? KYDrawerController
        drawerController?.setDrawerState(KYDrawerController.DrawerState.closed, animated: true)
    }

}
