//
//  DrawerMenuTableTableViewController.swift
//  Remedy
//
//  Created by macOS on 22.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import KYDrawerController

private let CELL_ID = "MenuCell"


private let MENU = "Main"
private let PROGRESS = "Progress"
private let PROFILE = "Profile"
private let RESEARCHES = "Researches"
private let SUPPORT = "Support"
private let IMT = "CalculatorVC"
private let SCORE = "CalculatorVCSCORE"
private let SCF = "CalculatorSKF"
private let СHAT = "ChatVC"
private let CALENDAR = "CalendarVC"
private let BONUSES = "Bonuses"
private let SEND_QUESTION = "Send_question"

class DrawerMenuTableViewController: UITableViewController {
    
    @IBOutlet weak var chatCell: MenuCell!
    //Чат
    var isOpen = false
//    let arraysControllers = [MENU, PROGRESS, PROFILE, RESEARCHES, SUPPORT, СHAT, CALENDAR, CONNECT]
    let arraysControllers = [MENU, PROGRESS, PROFILE, RESEARCHES, SUPPORT, СHAT, CALENDAR, IMT, SCORE, SCF, SEND_QUESTION]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(MenuCell.self, forCellReuseIdentifier: CELL_ID)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UIApplication.shared.applicationIconBadgeNumber == 0 {
            self.chatCell.chatBck.isHidden = true
            self.chatCell.messageCount.isHidden = true
        } else {
            self.chatCell.chatBck.isHidden = false
            self.chatCell.messageCount.isHidden = false
            self.chatCell.messageCount.text = "\(UIApplication.shared.applicationIconBadgeNumber)"
            self.chatCell.chatBck.layer.cornerRadius = self.chatCell.chatBck.frame.size.width / 2
            self.chatCell.chatBck.clipsToBounds = true
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let drawerController = navigationController?.parent as? KYDrawerController
        
        var vc = CustomNavigationController()
        
        switch indexPath.row {
        case 0:
            vc = (UIStoryboard(name: STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: MENU) as? CustomNavigationController)!
            vc.titleText = "Стрічка"
            break
        case 1:
            vc = (UIStoryboard(name: STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: RESEARCHES) as? CustomNavigationController)!
            vc.titleText = "Дослідження"
            break
        case 2:
            vc = (UIStoryboard(name: STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: BONUSES) as? CustomNavigationController)!
            vc.titleText = "Мій рейтинг"
            break
        case 3:
            vc = (UIStoryboard(name: STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: SUPPORT) as? CustomNavigationController)!
            vc.titleText = "Підтримка"
            break
        case 4:
            vc = (UIStoryboard(name: STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: PROFILE) as? CustomNavigationController)!
            vc.titleText = "Профіль"
            break
        case 5:
            vc = (UIStoryboard(name: STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: СHAT) as? CustomNavigationController)!
            vc.titleText = "Чат"
            break
        case 6:
            vc = (UIStoryboard(name: STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: CALENDAR) as? CustomNavigationController)!
            vc.titleText = "Календар"
            break
//        case 11:
//            vc = (UIStoryboard(name: STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: CONNECT) as? CustomNavigationController)!
//            vc.titleText = "CONNECT"
//            break
        case 7:
            self.update()
            return
        case 8:
            vc = (UIStoryboard(name: STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: IMT) as? CustomNavigationController)!
            vc.titleText = "Калькулятор"
            break
        case 9:
            vc = (UIStoryboard(name: STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: SCORE) as? CustomNavigationController)!
            vc.titleText = "Калькулятор"
            break
        case 10:
            vc = (UIStoryboard(name: STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: SCF) as? CustomNavigationController)!
            vc.titleText = "Калькулятор"
            break
        case 11:
            vc = (UIStoryboard(name: STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: SEND_QUESTION) as? CustomNavigationController)!
            vc.titleText = "Задати питання"
            break
        default:
            break
        }
        
        drawerController?.mainViewController = vc
        drawerController?.setDrawerState(KYDrawerController.DrawerState.closed, animated: true)
    }
    
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell") as! MenuCell
//        if indexPath.row == 5 {
//
//        }
//        return cell
//    }
    
    func update () {
        self.isOpen = !self.isOpen
        self.tableView.beginUpdates()
        self.tableView.reloadRows(at:[
                                    IndexPath(item: 6, section: 0),
                                    IndexPath(item: 7, section: 0),
                                    IndexPath(item: 8, section: 0)
            ], with: UITableView.RowAnimation.none)
        self.tableView.endUpdates()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 7 {
            return 0
        }
        if !isOpen {
            if indexPath.row == 8 || indexPath.row == 9 || indexPath.row == 10 {
                return 0
            }
        } else {
           return 59
        }
        return 59
    }
}
