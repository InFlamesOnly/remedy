//
//  RoundView.swift
//  Remedy
//
//  Created by macOS on 25.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func roundView () {
        self.layer.cornerRadius = self.bounds.height / 2
        self.layer.masksToBounds = true
    }
    
    func roundCorners (value : CGFloat) {
        self.layer.cornerRadius = value
        self.layer.masksToBounds = true
    }
}

