//
//  PhoneWithoutSymbolsExt.swift
//  Remedy
//
//  Created by macOS on 05.02.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import Foundation

extension String {
    func filterPhone () -> String {
        return self.filter { !" ()-".contains($0) }
    }
}
