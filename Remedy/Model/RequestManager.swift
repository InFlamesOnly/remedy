//
//  RequestManager.swift
//  Remedy
//
//  Created by macOS on 05.02.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import Alamofire

private enum Errors : String {
    case Password = "Неверный пароль!"
    case User = "Такого юзера не существует"
    case Code = "Неправильный код подтверждения!"
}

private let headers = ["Content-type": "application/x-www-form-urlencoded" , "Accept" : "application/json"]

private let kAuth = "auth"
private let kCode = "gencode"
private let kCheckCode = "checkcode"
private let kNewPassword = "resetpassword"
private let kAllResearch = "getalllists"
private let kWorksheets = "getmyworksheets"
private let kMyProfile = "getmyprofile"
private let kAcounts = "getmyauth"
private let kRegDevice = "reg_device"
private let kUserFromToken = "token_auth"
private let kFCMToken = "setFCMToken"
private let kClearDeviceId = "clear_nonauth"
private let kRemoveAcc = "del_account"
private let kContent = "get_content"
private let kPlusView = "plus_view"
// let request = String(format:"%@?token=%@",kCalendarDate,user.token)
private let kCalendarDate = "get_all_calendar"

private let kBonuses = "getmyworksheets"
private let historyList = "history_list"
private let historyBonus = "history_bonus"

private let kMessagess = "messages_count"

//ONLINE OFFLINE
private let kOnline = "online"
private let kOffline = "offline"

//let getList = String(format:"%@legal_consultation?token=%@",kAPIServer,user.token)
//let getList = String(format:"%@legal_consultation?token=%@&id=%@",kAPIServer,user.token, lcId)
private let kLegalConsultation = "legal_consultation"
private let kLegalConsultationFull = "show_consultation_full"
private let kSendQestions = "send_question"

//getLegalConsultation
//let getList = String(format:"%@legal_consultation?token=%@",kAPIServer,user.token)

//getMyLegalConsultation
//let getList = String(format:"%@legal_consultation?token=%@&popular=1",kAPIServer,user.token)

//getLegalConsultationFromId
//let getList = String(format:"%@legal_consultation?token=%@&id=%@",kAPIServer,user.token, lcId)

//func sendQestion (qestionId : Int, text : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
//
//    var parameters = ["question" : text,
//                      "token" : user.token,] as [String : Any]
//
//    if qestionId != 0 {
//        parameters.updateValue(qestionId, forKey: "question_id")
//    }
//    self.postRequest(request: kSendQestions, parameters: parameters, success: { (_ responseObject: Any) in
//        success(responseObject as! Dictionary<String, Any>)
//    }) { (_ error: Int?) in
//        guard let error = error else { return }
//        failure(self.checkErrorCode(code: error))
//    }
//}


class RequestManager: NSObject {
    
    static let shared = RequestManager()
    
}

extension RequestManager {
    
//    func sendQestion (qestionId : Int, text : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
//
//        var parameters = ["question" : text,
//                          "token" : user.token,] as [String : Any]
//
//        if qestionId != 0 {
//            parameters.updateValue(qestionId, forKey: "question_id")
//        }
//        self.postRequest(request: kSendQestions, parameters: parameters, success: { (_ responseObject: Any) in
//            success(responseObject as! Dictionary<String, Any>)
//        }) { (_ error: Int?) in
//            guard let error = error else { return }
//            failure(self.checkErrorCode(code: error))
//        }
//    }
    
    func sendQestion (qestionId : Int, text : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["question" : text,
                          "token" : User.currentUser().token]
        let request = "\(kAPIServer)\(kSendQestions)"
        self.post(request: request, parameters: parameters, success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            if errorCode == 200 {
                success(["" : ""])
                return
            }
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func questionList (id : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let request = "\(kAPIServer)\(kLegalConsultation)?token=\(User.currentUser().token)&id=\(id)"
        self.get(request: request, parameters: [:], success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            if errorCode == 200 {
                success(["" : ""])
                return
            }
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func questionMyList (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let request = "\(kAPIServer)\(kLegalConsultation)?token=\(User.currentUser().token)"
        self.get(request: request, parameters: [:], success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            if errorCode == 200 {
                success(["" : ""])
                return
            }
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func questionList (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let request = "\(kAPIServer)\(kLegalConsultation)?token=\(User.currentUser().token)&popular=1"
        self.get(request: request, parameters: [:], success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            if errorCode == 200 {
                success(["" : ""])
                return
            }
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func calendarDates (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let request = "\(kAPIServer)\(kCalendarDate)?token=\(User.currentUser().token)"
        self.get(request: request, parameters: [:], success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            if errorCode == 200 {
                success(["" : ""])
                return
            }
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }

    func offline (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let request = "https://chat.remedy-ua.com/index.php?act=leave_chat&token=\(User.currentUser().token)"
        //        let request = "\(kAPIServer)\(kOffline)?token=\(User.currentUser().token)"
        self.get(request: request, parameters: [:], success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            if errorCode == 200 {
                success(["" : ""])
                return
            }
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func auth (phone : String, password : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["user" : ["phone" : phone,
                                    "password" : password]]
        let request = "\(kAPIServer)\(kAuth)"
        self.post(request: request, parameters: parameters, success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            if errorCode == 200 {
                success(["" : ""])
                return
            }
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func registration (name : String, surname : String, phone : String, speciality: String, city: String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["user" : ["name" : name,
                                    "surname" : surname,
                                    "phone" : phone,
                                    "speciality" : speciality,
                                    "city" : city]]
        let request = "https://remedy-ua.com/sns.php"
        self.post(request: request, parameters: parameters, success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            if errorCode == 200 {
                success(["" : ""])
                return
            }
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func messagessCount (messagess: @escaping (_ bonusesText: Int) -> Void, failure: @escaping (_ error: Int?) -> Void?) {
        if User.userIsFind() {
            let request = "\(kAPIServer)\(kMessagess)?token=\(User.currentUser().token)"
            self.get(request: request, parameters: [:], success: { (responseObject) in
                messagess(self.messagessText(responseObject: responseObject))
            }) { (errorCode) in
                //            self.error(code: errorCode!)
                failure(0)
            }
        }
    }
    
//    - (void)getUserWorkSheetsWithBonusessText:(void(^)(NSString *text)) text
//                                    companyId:(void(^)(NSNumber *companyId)) companyId
//                                    bonuesURL:(void(^)(NSString *bonusesURL)) bonusesURL
//                              howUseBonuesURL:(void(^)(NSString *bonusesURL)) howUseBonues
    
    func getBonuses (success: @escaping (_ responseObject: Dictionary<String, Any>,
                                         _ text : String,
                                         _ companyId : Int,
                                         _ bonusesURL : String,
                                         _ howUseBonues : String) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        if User.userIsFind() {
//            let parameters = ["user" : ["token" : "vvvvdddfff"]]
//            let request = "https://anamnes.com.ua/api/v3/getmyworksheets"
            let parameters = ["user" : ["token" : User.currentUser().token]]
            let request = "\(kAPIServer)\(kBonuses)"
            
            self.post(request: request, parameters: parameters, success: { (responseObject) in
                let (companyId, text, bonusesURL, howUseBonuesURL) = self.getNeedFields(response: responseObject)
                
                success(responseObject, text, companyId, bonusesURL, howUseBonuesURL)
            }) { (errorCode) in
//                if errorCode == 200 {
//                    success(["" : ""])
//                    return
//                }
                self.error(code: errorCode!)
                failure(errorCode)
            }
        }
    }
    
    func getHistoryList (success: @escaping (_ text : String) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        if User.userIsFind() {
//            let parameters = ["user" : ["token" : "vvvvdddfff"]]
//            let request = "https://anamnes.com.ua/api/v3/history_list"
            let parameters = ["user" : ["token" : User.currentUser().token]]
            let request = "\(kAPIServer)\(historyList)"
            
            self.post(request: request, parameters: parameters, success: { (responseObject) in
                success(self.getHistoryListText(response: responseObject))
            }) { (errorCode) in
                self.error(code: errorCode!)
                failure(errorCode)
            }
        }
    }
    
    func getHistoryBonus (success: @escaping (_ text : String) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        if User.userIsFind() {
//            let parameters = ["user" : ["token" : "vvvvdddfff"]]
//            let request = "https://anamnes.com.ua/api/v3/history_bonus"
            let parameters = ["user" : ["token" : User.currentUser().token]]
            let request = "\(kAPIServer)\(historyBonus)"
            
            self.post(request: request, parameters: parameters, success: { (responseObject) in
                success(self.getHistoryListText(response: responseObject))
            }) { (errorCode) in
                self.error(code: errorCode!)
                failure(errorCode)
            }
        }
    }
    
    func getHistoryListText (response : Dictionary<String, Any>) -> String {
        var text = ""
        
        if let user = response["user"] as? [String: Any] {
            if let html = user["html"] as? String {
                text = html
            }
        }
        return text
    }
    
    func getNeedFields (response : Dictionary<String, Any>) -> (Int, String, String, String) {
        var text = ""
        var companyId = 0
        var bonusesURL = ""
        var howUseBonuesURL = ""
        
        if let user = response["user"] as? [String: Any] {
            if let company = user["compnay_id"] as? Int {
                companyId = company
            }
            
            if let userData = user["user_data"] as? [String: Any] {
                if let needText = userData["text"] as? String {
                    text = needText
                }
                
                if let bonuses = userData["bonus_url"] as? String {
                    bonusesURL = bonuses
                }
                
                if let howUseBonues = userData["how_use_bonus"] as? String {
                    howUseBonuesURL = howUseBonues
                }
            }
        }
        return (companyId, text, bonusesURL, howUseBonuesURL)
    }
    
    //static NSString *myWorkSheets = @"https://anamnes.com.ua/api/v3/getmyworksheets";
    
    func plusContent (article : Article, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["token" : User.currentUser().token,
                          "post_id" : article.id] as [String : Any]
        let request = "\(kAPIServer)\(kPlusView)"
        self.post(request: request, parameters: parameters, success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            if errorCode == 200 {
                success(["" : ""])
                return
            }
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func content (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let request = "\(kAPIServer)\(kContent)?token=\(User.currentUser().token)"
        self.get(request: request, parameters: [:], success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            if errorCode == 200 {
                success(["" : ""])
                return
            }
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func forgotPassword (phone : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["user" : ["phone" : phone]]
        let request = "\(kAPIServer)\(kCode)"
        self.post(request: request, parameters: parameters, success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            if errorCode == 200 {
                success(["" : ""])
                return
            }
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func checkCode (phone : String, code : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["user" : ["phone" : phone, "code" : code]]
        let request = "\(kAPIServer)\(kCheckCode)"
        self.post(request: request, parameters: parameters, success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            if errorCode == 200 {
                success(["" : ""])
                return
            }
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func newPassword (phone : String, code : String, newPassword : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["user" : ["phone" : phone, "code" : code, "new_password" : newPassword]]
        let request = "\(kAPIServer)\(kNewPassword)"
        self.post(request: request, parameters: parameters, success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            if errorCode == 200 {
                success(["" : ""])
                return
            }
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
}

extension RequestManager {
    func userFromToken (token : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["user" : ["token" : token]]
        let request = "\(kAPIServer)\(kUserFromToken)"
        self.post(request: request, parameters: parameters, success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            if errorCode == 200 {
                success(["" : ""])
                return
            }
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func researchList (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["user" : ["token" : User.currentUser().token]]
        let request = "\(kAPIServer)\(kAllResearch)"
        self.post(request: request, parameters: parameters, success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            if errorCode == 200 {
                success(["" : ""])
                return
            }
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func worksheetsInfo (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void,
                         bonusesText: @escaping (_ bonusesText: String) -> Void,
                         failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["user" : ["token" : User.currentUser().token]]
        let request = "\(kAPIServer)\(kWorksheets)"
        self.post(request: request, parameters: parameters, success: { (responseObject) in
            bonusesText(self.bonusesText(responseObject: responseObject))
            success(responseObject)
        }) { (errorCode) in
            if errorCode == 200 {
                success(["" : ""])
                return
            }
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func bonusesText (responseObject: Dictionary<String, Any>) -> String {
        var bonusesText = ""
        if let user = responseObject["user"] as? [String: Any] {
            if let userData = user["user_data"] as? [String: Any] {
                let text = userData["text"] as? String ?? ""
                bonusesText = text
            }
        }
        return bonusesText
    }
    
    func messagessText (responseObject: Dictionary<String, Any>) -> Int {
        var messagessCount = 0
        if let user = responseObject["user"] as? [String: Any] {
            if let userData = user["user_data"] as? [String: Any] {
                let count = userData["messages_count"] as? Int ?? 0
                messagessCount = count
            }
        }
        return messagessCount
    }
    
    func myProfile (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void,
                    failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["user" : ["token" : User.currentUser().token]]
        let request = "\(kAPIServer)\(kMyProfile)"
        self.post(request: request, parameters: parameters, success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            if errorCode == 200 {
                success(["" : ""])
                return
            }
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func setDevice (deviceToken : String, fcmToken : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["user" : ["device_id" : deviceToken,
                                    "fcm_token" : fcmToken,
                                    "device" : "IOS"]]
        let request = "\(kAPIServer)\(kRegDevice)"
        self.post(request: request, parameters: parameters, success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            if errorCode == 200 {
                success(["" : ""])
                return
            }
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func setFCM (fcmToken : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["user" : ["token" : User.currentUser().token, "fcmtoken" : fcmToken]]
        let request = "\(kAPIServer)\(kFCMToken)"
        self.post(request: request, parameters: parameters, success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            if errorCode == 200 {
                success(["" : ""])
                return
            }
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func remove (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["user" : ["token" : User.currentUser().token]]
        let request = "\(kAPIServer)\(kRemoveAcc)"
        self.post(request: request, parameters: parameters, success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            if errorCode == 200 {
                success(["" : ""])
                return
            }
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func clearDeviceId (deviceId : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["user" : ["device_id" : deviceId]]
        let request = "\(kAPIServer)\(kClearDeviceId)"
        self.post(request: request, parameters: parameters, success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            if errorCode == 200 {
                success(["" : ""])
                return
            }
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func acoounts (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void,
                    failure: @escaping (_ error: Int?) -> Void) {
//        let parameters = ["user" : ["token" : User.currentUser().token]]
//        NSString *request = [NSString stringWithFormat:@"%@?token=%@",getUsers,user.token];
        let request = "\(kAPIServer)\(kAcounts)?token=\(User.currentUser().token)"
        self.get(request: request, parameters: [:], success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            if errorCode == 200 {
                success(["" : ""])
                return
            }
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
}

extension RequestManager {
    private func error (code : Int) {
        switch code  {
        case 201:
            self.showError(message: Errors.Password.rawValue)
            break
        case 202:
            self.showError(message: Errors.User.rawValue)
            break
        case 203:
            self.showError(message: Errors.Code.rawValue)
            break
        default:
            break
        }
    }
    
    private func showError (message : Errors.RawValue) {
        if let topController = UIApplication.topViewController() {
            topController.presentAlert(withTitle: "Ошибка", message: message)
        }
    }
}

extension RequestManager {
    private func get (request : String , parameters: [AnyHashable: Any], success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if response.response?.statusCode == 200 {
                    success(response.result.value! as! Dictionary<String, Any>)
                } else {
                    failure(response.response?.statusCode)
                }
                print(response.result.value!)
                break
            case .failure(_):
                failure(response.response?.statusCode)
                print(response.result.error!)
                break
            }
        }
    }
    
    private func delete (request : String , parameters: [String: Any], success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        Alamofire.request(request, method: .delete, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if response.response?.statusCode == 200 {
                    success(response.result.value! as! Dictionary<String, Any>)
                } else {
                    failure(response.response?.statusCode)
                }
                print(response.result.value!)
                break
            case .failure(_):
                failure(response.response?.statusCode)
                print(response.result.error!)
                break
            }
        }
    }
    
    private func put (request : String , parameters: [String: Any], success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        Alamofire.request(request, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if response.response?.statusCode == 200 {
                    success(response.result.value! as! Dictionary<String, Any>)
                } else {
                    failure(response.response?.statusCode)
                }
                print(response.result.value!)
                break
            case .failure(_):
                failure(response.response?.statusCode)
                print(response.result.error!)
                break
            }
        }
    }
    
    private func post (request : String , parameters: [String: Any], success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        
        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if response.response?.statusCode == 200 {
                    success(response.result.value! as! Dictionary<String, Any>)
                } else {
                    failure(response.response?.statusCode)
                }
                print(response.result.value!)
                break
            case .failure(_):
                failure(response.response?.statusCode)
                print(response.result.error!)
                break
            }
        }
    }
}
