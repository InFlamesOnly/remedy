//
//  EndResearch.swift
//  Remedy
//
//  Created by macOS on 07.02.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import RealmSwift

class Worksheets: Object {
    
    @objc dynamic var status = 0
    @objc dynamic var text = ""
    @objc dynamic var name = ""
    @objc dynamic var date = ""
    
    func get (response : Dictionary<String, Any>) {
        self.status = response ["status"] as? Int ?? 0
        self.name = response ["name"] as? String ?? ""
        self.text = response ["text"] as? String ?? ""
        self.date = response ["date"] as? String ?? ""
    }
    
    class func mapResponseToArrayObject (response : Dictionary<String, Any>) -> Array<Worksheets> {
        var array = Array<Worksheets>()
        if let user = response["user"] as? [String: Any] {
            if let userData = user["user_data"] as? [String: Any] {
                if let arrayObj = userData["worksheets"] as? [[String:Any]] {
                    for arrayDict in arrayObj {
                        let object = Worksheets()
                        object.get(response: arrayDict)
                        array.append(object)
                        
                    }
                }
            }
        }
        return array
    }
}
