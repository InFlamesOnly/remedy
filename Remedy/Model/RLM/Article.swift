//
//  Article.swift
//  Remedy
//
//  Created by Dima on 16.04.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import RealmSwift

class Article: Object {
    
    @objc dynamic var id = 0
    @objc dynamic var tittle = ""
    @objc dynamic var content = ""
    @objc dynamic var shortContent = ""
    @objc dynamic var imagePath = ""
    @objc dynamic var category = ""
    @objc dynamic var categoryId = 0
    @objc dynamic var author = ""
    @objc dynamic var link = ""
    
    
    func get (response : Dictionary<String, Any>) {
        self.id = response ["article_id"] as? Int ?? 0
        self.tittle = response ["title"] as? String ?? ""
        self.content = response ["content"] as? String ?? ""
        self.shortContent = response ["short_content"] as? String ?? ""
        self.imagePath = response ["image"] as? String ?? ""
        self.category = response ["category"] as? String ?? ""
        self.categoryId = response ["category_id"] as? Int ?? 0
        self.author = response ["author"] as? String ?? ""
        self.link = response ["link"] as? String ?? ""
    }
    
    class func mapResponseToArrayObject (response : Dictionary<String, Any>) -> Array<Article> {
        var array = Array<Article>()
            if let arrayObj = response["result"] as? [[String:Any]] {
                for arrayDict in arrayObj {
                    let object = Article()
                    object.get(response: arrayDict)
                    array.append(object)
                    
            }
        }
        return array
    }
    
}

//"article_id": 39,
//"title": "Только ремеди",
//"content": "\n<figure class=\"wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-4-3 wp-has-aspect-ratio\"><div class=\"wp-block-embed__wrapper\">\n<iframe width=\"500\" height=\"375\" src=\"https://www.youtube.com/embed/rpkMEMLDr2s?feature=oembed\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>\n</div></figure>\n",
//"image": false,
//"category": "Новости",
//"category_id": 3,
//"author": "Remedy"
