//
//  User.swift
//  Remedy
//
//  Created by macOS on 06.02.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import RealmSwift

class User: Object {
    @objc dynamic var name = ""
    @objc dynamic var phone = ""
    @objc dynamic var token = ""
    @objc dynamic var firebaseToken = ""
    @objc dynamic var deviceToken = ""
    @objc dynamic var isAuth = 0
    
    @objc dynamic var bonuses = "0"
    @objc dynamic var notification = 1
    

    func get (response : Dictionary<String, Any>) {
        let user = (response as [String : AnyObject])["user"]
        let firstName = user! ["name"] as? String ?? ""
        let lastName = user! ["middle_name"] as? String ?? ""
        self.name = "\(firstName) \(lastName)"
        self.token = user! ["token"] as? String ?? ""
        self.phone = user! ["phone"] as? String ?? ""
        
    }
    
    func updateBonuses (response : Dictionary<String, Any>) {
        let userResponse = (response as [String : AnyObject])["user"]
        let userData = userResponse!["user_data"] as? [String: Any]
        let user = User.currentUser()
        let realm = try! Realm()
        try! realm.write {
            user.bonuses = userData! ["bonuses"] as? String ?? ""
        }
    }
    
    func notificationOn () {
        let user = User.currentUser()
        let realm = try! Realm()
        try! realm.write {
            user.notification = 1
        }
    }
    
    func notificationOff () {
        let user = User.currentUser()
        let realm = try! Realm()
        try! realm.write {
            user.notification = 0
        }
    }
    
    func updateFCMToken (firebaseToken : String) {
        let user = User.currentUser()
        let realm = try! Realm()
        try! realm.write {
            user.firebaseToken = firebaseToken
        }
    }
    
    func updateDeviceToken (deviceToken : String) {
        let user = User.currentUser()
        let realm = try! Realm()
        try! realm.write {
            user.deviceToken = deviceToken
        }
    }
    
    class func currentUser () -> User {
        let realm = try! Realm()
        return realm.objects(User.self)[0]
    }
    
    class func userIsFind () -> Bool {
        let realm = try! Realm()
        if realm.objects(User.self).count == 1 {
            return true
        } else {
            return false
        }
    }
    
    func save () {
        let realm = try! Realm()
        if realm.objects(User.self).count == 0 {
            try! realm.write {
                realm.add(self)
            }
        }
    }
    
    class func remove () {
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
    }
    
    func isAuth (auth : Bool) {
        let realm = try! Realm()
        if realm.objects(User.self).count == 1 {
            let user = User.currentUser()
            if auth {
                try! realm.write {
                    user.isAuth = 1
                }
            } else {
                try! realm.write {
                    user.isAuth = 0
                }
            }
        }
    }
}
