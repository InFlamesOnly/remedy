//
//  Account.swift
//  Remedy
//
//  Created by macOS on 11.02.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import RealmSwift

//"id": "2",
//"phone": "+380503825605",
//"token": "zCBZ6OlU4ty2mkXC61fVuVUabl8q8qeU12gstVIqxAwdYNeU",
//"name": "Исследование PRIM: оценка восстановления энергетической эффективности клеток у пациентов с кардио-церебральной патологией"

class Account: Object {
    @objc dynamic var id = ""
    @objc dynamic var phone = ""
    @objc dynamic var token = ""
    @objc dynamic var name = ""
    
    func get (response : Dictionary<String, Any>) {
        self.id = response ["id"] as? String ?? ""
        self.name = response ["name"] as? String ?? ""
        self.token = response ["token"] as? String ?? ""
        self.phone = response ["phone"] as? String ?? ""
    }
    
    class func mapResponseToArrayObject (response : Dictionary<String, Any>) -> Array<Account> {
        var array = Array<Account>()
        if let arrayObj = response["data"] as? [[String:Any]] {
            for arrayDict in arrayObj {
                let object = Account()
                object.get(response: arrayDict)
                array.append(object)
            }
        }
        return array
    }
}
