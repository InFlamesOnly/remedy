//
//  Constants.swift
//  Remedy
//
//  Created by macOS on 22.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import Foundation
import UIKit

let kAPIServer = "https://remedy-ua.com/api/v1/"

let STORYBOARD = "Main"

let FONT = "Roboto-Regular"
let FONT_MEDIUM = "Roboto-Medium"
let FONT_BOLD = "Roboto-Bold"

let FIRST_GRADIENT_COLOR = UIColor(red: 96/255, green: 110/255, blue: 247/255, alpha: 1)
let SECOND_GRADIENT_COLOR = UIColor(red: 43/255, green: 56/255, blue: 185/255, alpha: 1)

let SHADOW_COLOR = UIColor(red: 110/255, green: 110/255, blue: 110/255, alpha: 1)

let VIOLET_COLOR = UIColor(red: 96/255, green: 110/255, blue: 247/255, alpha: 1)

let RED_COLOR = UIColor(red: 218/255, green: 54/255, blue: 20/255, alpha: 1)
let GREEN_COLOR = UIColor(red: 17/255, green: 159/255, blue: 56/255, alpha: 1)
let YELLOW_COLOR = UIColor(red: 245/255, green: 166/255, blue: 35/255, alpha: 1)
